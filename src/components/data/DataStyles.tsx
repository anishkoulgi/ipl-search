import styled from 'styled-components';

export const ItemContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 576px) {
    flex-direction: row;
  }
`;

export const Item = styled.div<{ ind: number }>`
  padding: 15px;
  background: ${(props) => props.theme.colors.white};
  box-shadow: 2px 2px 10px 2px rgba(213, 213, 213, 0.25);
  border-radius: 10px;
  margin: 10px 0px;
  flex: 1;

  @media (min-width: 576px) {
    margin-left: ${(props) => (props.ind % 2 ? '10px' : '0px')};
    margin-right: ${(props) => (props.ind % 2 ? '0px' : '10px')};
  }
`;

export const StatsContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

export const BowlingContainer = styled.div`
  display: flex;
  margin-top: 5px;
  @media (min-width: 768px) {
    margin-top: 0px;
    margin-left: 5px;
  }
`;
