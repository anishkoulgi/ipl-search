import React from 'react';
import moment from 'moment';

import { Container, MyImage, MyText } from '../../reusables';
import { PlayersType } from '../../types';
import { getCountryFlag, theme } from '../../utils';
import { BowlingContainer, Item, StatsContainer } from './DataStyles';

export const PlayerElement: React.FC<{ item: PlayersType; index: number }> = ({
  item,
  index,
}) => {
  return (
    <Item ind={index}>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <MyText
          styles={{
            color: theme.colors.darkPrimary,
            fontWeight: 500,
            fontSize: '18px',
          }}
        >
          {item.Player_Name}
        </MyText>
        <MyImage
          src={getCountryFlag(item.Country)}
          height='30px'
          width='30px'
        />
      </Container>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: '10px',
        }}
      >
        <StatsContainer>
          <Container styles={{ display: 'flex' }}>
            <MyImage src='/images/batsman.svg' height='15px' width='15px' />
            <MyText
              styles={{
                color: theme.colors.darkGray,
                fontSize: '13px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.Batting_Hand || 'NA'}
            </MyText>
          </Container>
          <BowlingContainer>
            <MyImage src='/images/bowler.svg' height='15px' width='15px' />
            <MyText
              styles={{
                color: theme.colors.darkGray,
                fontSize: '13px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.Bowling_Skill || 'NA'}
            </MyText>
          </BowlingContainer>
        </StatsContainer>
        <MyText styles={{ color: theme.colors.lightGray }}>
          <Container styles={{ display: 'flex', alignItems: 'center' }}>
            Age :
            <MyText
              styles={{
                color: theme.colors.mediumGray,
                fontSize: '14px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.DOB ? moment().diff(item.DOB, 'years') : 'NA'}
            </MyText>
          </Container>
        </MyText>
      </Container>
    </Item>
  );
};
