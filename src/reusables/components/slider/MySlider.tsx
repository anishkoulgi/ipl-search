import { withStyles } from '@material-ui/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import Slider from '@material-ui/core/Slider';
import { theme } from '../../../utils';

export const MySlider = withStyles({
  root: {
    color: theme.colors.primary,
    height: 4,
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50%)',
    fontFamily: 'Karla, sans-serif',
  },
  track: {
    height: 4,
    borderRadius: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
})(Slider);

export default Slider;
