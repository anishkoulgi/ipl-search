import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

import { Container, MyText } from '..';
import { useOutsideClick } from '../../';
import { checkSelected, theme } from '../../../utils';
import {
  ButtonContainer,
  DropContainer,
  DropDownContainer,
  Line,
  Option,
  OptionsContainer,
} from './DropDownElements';

export type dropdown = {
  title: string;
  selected: boolean;
};

interface Props {
  data: dropdown[];
  header: string;
  LeftIcon?: () => JSX.Element;
  RightIcon?: () => JSX.Element;
  setData: (val: dropdown[]) => void;
  OptionsComponent: (props: { item: dropdown; index: number }) => JSX.Element;
  SelectedOptionsComponent: (props: {
    item: dropdown;
    index: number;
  }) => JSX.Element;
}

const DropDown: React.FC<Props> = ({
  data,
  header,
  LeftIcon,
  RightIcon,
  setData,
  OptionsComponent,
  SelectedOptionsComponent,
}) => {
  const { open, setOpen, node } = useOutsideClick();

  const handleChange = (i: number, value: boolean) => {
    let arr: any = [];
    let temp = arr.concat(data);
    temp[i].selected = value;
    setData(temp);
  };

  const clearAll = () => {
    let arr: any = [];
    let temp = arr.concat(data);
    temp.map((_: any, i: number) => (temp[i].selected = false));
    setData(temp);
  };

  const isEmpty = checkSelected(data);

  return (
    <DropContainer ref={node}>
      <DropDownContainer
        style={{
          borderColor:
            !isEmpty && !open
              ? theme.colors.veryLightGray
              : theme.colors.primary,
          cursor: 'pointer',
        }}
        onClick={() => setOpen((prev) => !prev)}
        open={open}
      >
        {LeftIcon && LeftIcon()}
        <MyText
          styles={{
            color: theme.colors.darkGray,
            flex: 1,
            margin: '0px 10px',
            display: 'flex',
          }}
        >
          {header}{' '}
          <MyText styles={{ fontSize: '13px', marginLeft: '10px' }}>
            {isEmpty !== 0 && `(${isEmpty} selected)`}
          </MyText>
        </MyText>
        {RightIcon ? (
          RightIcon()
        ) : (
          <FontAwesomeIcon
            color={
              !isEmpty && !open ? theme.colors.lightGray : theme.colors.primary
            }
            icon={open ? faChevronUp : faChevronDown}
          />
        )}
      </DropDownContainer>
      {open && (
        <OptionsContainer open={open}>
          <Scrollbars style={{ width: '100%', height: '200px' }}>
            <Container>
              <MyText
                styles={{
                  fontWeight: 500,
                  color: theme.colors.mediumGray,
                  padding: '10px',
                }}
              >
                SELECTED
              </MyText>
              {!isEmpty && (
                <MyText
                  styles={{
                    color: theme.colors.lightGray,
                    padding: '10px 10px 0px 10px',
                  }}
                >
                  None
                </MyText>
              )}
              {data.map((option, i) => {
                return option.selected ? (
                  <Option
                    key={i}
                    onClick={(e) => {
                      e.stopPropagation();
                      handleChange(i, false);
                    }}
                  >
                    {OptionsComponent({ item: option, index: i })}
                  </Option>
                ) : null;
              })}
            </Container>
            <Line />
            {data.map((option, i) => {
              return !option.selected ? (
                <Option
                  key={i}
                  onClick={(e) => {
                    e.stopPropagation();
                    handleChange(i, true);
                  }}
                >
                  {SelectedOptionsComponent({ item: option, index: i })}
                </Option>
              ) : null;
            })}
          </Scrollbars>
          <Line />
          <ButtonContainer>
            <MyText
              styles={{
                fontSize: 14,
                color: theme.colors.danger,
                cursor: 'pointer',
              }}
              onClick={clearAll}
            >
              {isEmpty ? 'Clear' : ' '}
            </MyText>
            <MyText
              styles={{
                fontSize: 14,
                color: theme.colors.primary,
                cursor: 'pointer',
              }}
              onClick={() => setOpen(false)}
            >
              Done
            </MyText>
          </ButtonContainer>
        </OptionsContainer>
      )}
    </DropContainer>
  );
};

export default DropDown;
