import {
  faHandPointLeft,
  faHandPointRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withStyles } from '@material-ui/core';
import React from 'react';
import Slider from '@material-ui/core/Slider';

import { Container, DropDown, MyImage, MyText } from '../../../reusables';
import { dropdown } from '../../../reusables/components/dropdown/DropDown';
import { PlayerFiltersType } from '../../../types';
import { getCountryFlag, theme } from '../../../utils';
import { HandContainer } from '../FilterElements';

export const MySlider = withStyles({
  root: {
    color: theme.colors.primary,
    height: 4,
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50%)',
    fontFamily: 'Karla, sans-serif',
  },
  track: {
    height: 4,
    borderRadius: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
})(Slider);

interface Props {
  playerFilterState: PlayerFiltersType;
  setPlayerFilterState: (playerFilters: PlayerFiltersType) => void;
}

const PlayerFilters: React.FC<Props> = ({
  playerFilterState,
  setPlayerFilterState,
}) => {
  const { age, bowlingStyle, countries, hand } = playerFilterState;

  const setHand = (val: number) => {
    setPlayerFilterState({
      ...playerFilterState,
      hand: val,
    });
  };

  const setCountries = (val: dropdown[]) => {
    setPlayerFilterState({
      ...playerFilterState,
      countries: val,
    });
  };

  const setBowlingStyle = (val: dropdown[]) => {
    setPlayerFilterState({
      ...playerFilterState,
      bowlingStyle: val,
    });
  };

  const handleChange = (event: React.ChangeEvent<{}>, value: any) => {
    setPlayerFilterState({
      ...playerFilterState,
      age: value,
    });
  };

  const handleHandChange = (i: number) => {
    if (hand === i) setHand(0);
    else setHand(i);
  };

  return (
    <>
      <Container styles={{ padding: '0px 15px', margin: '20px 0px 20px 0px' }}>
        <MyText styles={{ color: theme.colors.mediumGray }}>
          Age ({age[0]} - {age[1]})
        </MyText>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={age}
          onChange={handleChange}
          aria-labelledby='range-slider'
          min={15}
          max={45}
          marks={[
            { value: 15, label: '15' },
            { value: 45, label: '45' },
          ]}
        />
      </Container>
      <DropDown
        data={countries}
        header='Countries'
        setData={setCountries}
        LeftIcon={() => (
          <MyImage src={getCountryFlag('Country')} height='20px' width='20px' />
        )}
        OptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyImage
                src={getCountryFlag(item.title)}
                height='20px'
                width='20px'
              />
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/minus.svg' height='15px' width='15px' />
          </>
        )}
        SelectedOptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyImage
                src={getCountryFlag(item.title)}
                height='20px'
                width='20px'
              />
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/plus.svg' height='15px' width='15px' />
          </>
        )}
      />
      <Container styles={{ marginTop: '30px' }}>
        <DropDown
          data={bowlingStyle}
          header='Bowling Skills'
          setData={setBowlingStyle}
          LeftIcon={() => (
            <MyImage src='/images/bowler.svg' height='20px' width='20px' />
          )}
          OptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/minus.svg' height='15px' width='15px' />
            </>
          )}
          SelectedOptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/plus.svg' height='15px' width='15px' />
            </>
          )}
        />
      </Container>
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '30px' }}>
        Batting Hand
      </MyText>
      <Container styles={{ marginTop: '10px', display: 'flex' }}>
        <HandContainer
          style={{
            border: `1px solid ${
              hand === 1 ? theme.colors.primary : theme.colors.lightGray
            }`,
            marginRight: '10px',
            cursor: 'pointer',
          }}
          onClick={() => handleHandChange(1)}
        >
          <FontAwesomeIcon
            color={hand !== 1 ? theme.colors.lightGray : theme.colors.primary}
            icon={faHandPointLeft}
          />
          <MyText
            styles={{
              color:
                hand !== 1 ? theme.colors.mediumGray : theme.colors.primary,
              marginLeft: '10px',
            }}
          >
            Left Handed
          </MyText>
        </HandContainer>
        <HandContainer
          style={{
            border: `1px solid ${
              hand === 2 ? theme.colors.primary : theme.colors.lightGray
            }`,
            marginLeft: '10px',
            cursor: 'pointer',
          }}
          onClick={() => handleHandChange(2)}
        >
          <FontAwesomeIcon
            color={hand !== 2 ? theme.colors.lightGray : theme.colors.primary}
            icon={faHandPointRight}
          />
          <MyText
            styles={{
              color:
                hand !== 2 ? theme.colors.mediumGray : theme.colors.primary,
              marginLeft: '10px',
            }}
          >
            Right Handed
          </MyText>
        </HandContainer>
      </Container>
    </>
  );
};

export default PlayerFilters;
