import styled from 'styled-components';

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

export const BodyContainer = styled.div`
  background-color: ${(props) => props.theme.colors.faintGray};
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
`;
