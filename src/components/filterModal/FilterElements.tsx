import styled from 'styled-components';

export const HandContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 10px 0px;
  border-radius: 5px;
`;

export const ModalContainer = styled.div`
  height: 95%;
  width: 100%;
  padding: 10px;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  border: 1px solid ${(props) => props.theme.colors.veryLightGray};
  background-color: ${(props) => props.theme.colors.white};
  border-radius: 10px;

  @media (min-width: 576px) {
    width: 60%;
  }

  @media (min-width: 768px) {
    width: 50%;
  }

  @media (min-width: 1280px) {
    width: 30%;
  }
`;

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
  position: relative;
`;
