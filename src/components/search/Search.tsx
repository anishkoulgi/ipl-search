import React from 'react';
import { MyButton, MyImage, MyText } from '../../reusables/components';
import { getDataKeyName, theme } from '../../utils';
import { SearchContainer, SearchInput } from './SearchStyles';

const Search = ({ tab }: { tab: number }) => {
  return (
    <SearchContainer>
      <MyImage src='/images/search.svg' height='20px' width='20px' />
      <SearchInput
        type='text'
        placeholder={`Search for ${getDataKeyName(tab)}`}
      />
      <MyButton
        style={{
          height: '100%',
          borderRadius: 0,
          borderTopRightRadius: '20px',
          borderBottomRightRadius: '20px',
          padding: '0px 15px',
        }}
      >
        <MyText styles={{ color: theme.colors.white }}>Search</MyText>
      </MyButton>
    </SearchContainer>
  );
};

export default Search;
