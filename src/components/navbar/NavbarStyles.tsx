import styled from 'styled-components';

interface Props {
  scrolled: boolean;
}

export const Nav = styled.nav`
  display: flex;
  max-width: 1200px;
  padding: 10px 20px;
  justify-content: space-between;
  width: 100%;
`;

export const NavContainer = styled.div<Props>`
  display: flex;
  justify-content: center;
  width: 100%;
  box-shadow: ${(props) =>
    props.scrolled ? '0px 1px 15px rgba(8, 27, 77, 0.16)' : ''};
  transition: all 500ms;
  position: fixed;
  top: 0;
  left: 0;
  height: 60px;
  z-index: 999;
  background-color: ${(props) => props.theme.colors.white};
`;
