import React from 'react';
import { MyText } from '../../reusables';
import { theme } from '../../utils';
import { JumboContainer } from './JumbotronStyles';

const Jumbotron = () => {
  return (
    <JumboContainer>
      <MyText
        styles={{ fontSize: 40, color: theme.colors.primary, fontWeight: 500 }}
      >
        IPL Search
      </MyText>
      <MyText
        styles={{
          fontSize: 15,
          color: theme.colors.mediumGray,
          textAlign: 'center',
          marginTop: '20px',
        }}
      >
        Search through the IPL data for your favorite players, teams, owners and
        venues.
      </MyText>
    </JumboContainer>
  );
};

export default Jumbotron;
