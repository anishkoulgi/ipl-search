import React from 'react';
import { Container, MyText } from '../../reusables';
import { OwnerType } from '../../types';
import { theme } from '../../utils';
import { Item, StatsContainer } from './DataStyles';

export const OwnerElement: React.FC<{ item: OwnerType; index: number }> = ({
  item,
  index,
}) => {
  return (
    <Item ind={index}>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <MyText
          styles={{
            color: theme.colors.darkPrimary,
            fontWeight: 500,
            fontSize: '18px',
            flex: 2,
          }}
        >
          {item.name}
        </MyText>
        <MyText styles={{ color: theme.colors.lightGray, flex: 1 }}>
          <Container
            styles={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            Net Worth :
            <MyText
              styles={{
                color: theme.colors.mediumGray,
                fontSize: '14px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.netWorth}
            </MyText>
          </Container>
        </MyText>
      </Container>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: '10px',
        }}
      >
        <StatsContainer>
          <MyText
            styles={{
              color: theme.colors.darkGray,
              fontSize: '13px',
              fontWeight: 500,
              marginLeft: '5px',
            }}
          >
            Type : {item.type}
          </MyText>
        </StatsContainer>
        <MyText styles={{ color: theme.colors.lightGray }}>
          <Container styles={{ display: 'flex', alignItems: 'center' }}>
            Owner Since :
            <MyText
              styles={{
                color: theme.colors.mediumGray,
                fontSize: '14px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.ownerSince}
            </MyText>
          </Container>
        </MyText>
      </Container>
    </Item>
  );
};
