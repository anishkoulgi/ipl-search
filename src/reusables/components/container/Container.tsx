import React from 'react';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  styles?: React.CSSProperties;
}

const Container: React.FC<Props> = ({ children, styles, ...rest }) => {
  return (
    <div style={{ transition: 'all 500ms', ...styles }} {...rest}>
      {children}
    </div>
  );
};

export default Container;
