export const theme = {
  colors: {
    primary: '#3269FE',
    text: '#081B4D',
    darkPrimary: '#005FE6',
    success: '#43CD71',
    danger: '#ED4F32',
    lightGray: '#BBBBBB',
    mediumGray: '#999999',
    darkGray: '#676767',
    faintGray: '#FAFCFD',
    white: '#fff',
    veryLightGray: '#E5E5E5',
  },
};
