import styled from 'styled-components';

export const DropContainer = styled.div`
  position: relative;
  width: 100%;
`;

export const DropDownContainer = styled.div<{ open: boolean }>`
  display: flex;
  align-items: center;
  border: 1px solid ${(props) => props.theme.colors.primary};
  border-radius: 5px;
  border-bottom-left-radius: ${(props) => (props.open ? '0px' : '5px')};
  border-bottom-right-radius: ${(props) => (props.open ? '0px' : '5px')};
  padding: 0px 10px;
  height: 35px;
`;

export const OptionsContainer = styled.div<{ open: boolean }>`
  position: absolute;
  top: 35px;
  left: 0;
  z-index: 999;
  background-color: ${(props) => props.theme.colors.white};
  width: 100%;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.colors.primary};
  border-top-left-radius: ${(props) => (props.open ? '0px' : '5px')};
  border-top-right-radius: ${(props) => (props.open ? '0px' : '5px')};
`;

export const Option = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 7px 10px;
  :hover {
    background-color: ${(props) => props.theme.colors.veryLightGray};
  }
  transition: all 200ms;
  cursor: pointer;
`;

export const Line = styled.div`
  height: 1px;
  background-color: ${(props) => props.theme.colors.veryLightGray};
  margin: 10px;
`;

export const ButtonContainer = styled.div`
  display: flex;
  padding: 10px;
  justify-content: space-between;
`;
