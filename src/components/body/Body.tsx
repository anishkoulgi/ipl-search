import React, { useState } from 'react';
import { Search } from '..';
import { Container, MyButton, MyImage, MyText } from '../../reusables';
import { FilterStateType } from '../../types';
import {
  getDataKeyName,
  getOwnerFilterState,
  getPlayerFilterState,
  getTeamFilterState,
  getVenueFilterState,
  theme,
} from '../../utils';
import FilterModal from '../filterModal/FilterModal';
import {
  BodyContainer,
  FilterDesktopContainer,
  SearchContainer,
} from './BodyStyles';

const Body = ({ tab }: { tab: number }) => {
  const [open, setOpen] = useState(false);

  const SavedState = {
    player: localStorage.getItem('playerFilters'),
    team: localStorage.getItem('teamFilters'),
    owner: localStorage.getItem('ownerFilters'),
    venue: localStorage.getItem('venueFilters'),
  };

  const [applied, setApplied] = useState({
    players: false,
    teams: false,
    owners: false,
    venues: false,
  });

  const [filterState, setFilterState] = useState<FilterStateType>({
    playerFilters: SavedState.player
      ? JSON.parse(SavedState.player)
      : getPlayerFilterState(),
    teamFilters: SavedState.team
      ? JSON.parse(SavedState.team)
      : getTeamFilterState(),
    ownerFilters: SavedState.owner
      ? JSON.parse(SavedState.owner)
      : getOwnerFilterState(),
    venueFilters: SavedState.venue
      ? JSON.parse(SavedState.venue)
      : getVenueFilterState(),
  });

  const closeModal = () => setOpen(false);

  return (
    <BodyContainer>
      <SearchContainer>
        <Search tab={tab} />
        <FilterDesktopContainer>
          <MyButton
            style={{
              display: 'inline-block',
              padding: '8px 15px',
              marginTop: '10px',
              marginLeft: '20px',
            }}
            onClick={() => setOpen(true)}
          >
            <Container
              styles={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <MyImage src='/images/filter.svg' height='15px' width='15px' />
              <MyText styles={{ marginLeft: '5px', color: theme.colors.white }}>
                Filter
              </MyText>
            </Container>
          </MyButton>
        </FilterDesktopContainer>
      </SearchContainer>
      {applied[getDataKeyName(tab)] && (
        <MyText
          styles={{
            color: theme.colors.mediumGray,
            fontSize: '15px',
            marginTop: '20px',
          }}
        >
          Searched by applying Filters
        </MyText>
      )}
      <FilterModal
        isOpen={open}
        closeModal={closeModal}
        tab={tab}
        filterState={filterState}
        setFilterState={setFilterState}
        setApplied={setApplied}
      />
    </BodyContainer>
  );
};

export default Body;
