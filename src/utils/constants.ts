export const CategoryTabs = ['Players', 'Teams', 'Owners', 'Venues'];

export const Countries = [
  'India',
  'South Africa',
  'Australia',
  'England',
  'West Indies',
  'Sri Lanka',
  'Bangladesh',
  'New Zealand',
];

export const BowlingStyles = [
  'Right-arm medium',
  'Right-arm offbreak',
  'Left-arm fast-medium',
  'Right-arm fast-medium',
  'Right-arm fast',
  'Legbreak googly',
  'Legbreak',
  'Left-arm medium-fast',
  'Slow left-arm orthodox',
  'Right-arm medium-fast',
  'Left-arm medium',
  'Left-arm fast',
  'Slow left-arm chinaman',
  'Right-arm bowler',
  'Right-arm Medium',
  'Right-arm Fast',
];

export const Titles = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  '11',
  '12',
];

export const Matches = [
  'Less than 50',
  'Between 50 to 100',
  'Between 100 to 200',
  'Greater than 200',
];

export const OwnershipYears = [
  '2008',
  '2009',
  '2010',
  '2011',
  '2012',
  '2013',
  '2014',
  '2015',
  '2016',
  '2017',
  '2018',
  '2019',
  '2020',
];

export const capacities = [
  'Less than 25000',
  'Between 25000 to 50000',
  'Between 50000 to 100000',
  'Greater than 100000',
];
