import { faUser, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withStyles } from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import React from 'react';
import { Container, DropDown, MyImage, MyText } from '../../../reusables';
import { dropdown } from '../../../reusables/components/dropdown/DropDown';
import { OwnerFiltersType } from '../../../types';
import { theme } from '../../../utils';
import { HandContainer } from '../FilterElements';

export const MySlider = withStyles({
  root: {
    color: theme.colors.primary,
    height: 4,
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50%)',
    fontFamily: 'Karla, sans-serif',
  },
  track: {
    height: 4,
    borderRadius: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
  markLabel: {
    fontSize: '11px',
    color: theme.colors.mediumGray,
  },
})(Slider);

interface Props {
  ownerFilterState: OwnerFiltersType;
  setOwnerFilterState: (playerFilters: OwnerFiltersType) => void;
}

const OwnerFilters: React.FC<Props> = ({
  ownerFilterState,
  setOwnerFilterState,
}) => {
  const { netWorth, ownershipSince, ownershipType } = ownerFilterState;

  const setNetWorth = (e: any, val: any) => {
    setOwnerFilterState({
      ...ownerFilterState,
      netWorth: val,
    });
  };

  const setOwnershipSince = (val: dropdown[]) => {
    setOwnerFilterState({
      ...ownerFilterState,
      ownershipSince: val,
    });
  };

  const setOwnershipType = (val: number) => {
    setOwnerFilterState({
      ...ownerFilterState,
      ownershipType: val,
    });
  };

  const handleHandChange = (i: number) => {
    if (ownershipType === i) setOwnershipType(0);
    else setOwnershipType(i);
  };

  return (
    <>
      <Container styles={{ padding: '0px 15px', margin: '20px 0px 20px 0px' }}>
        <MyText styles={{ color: theme.colors.mediumGray }}>
          Net Worth ({netWorth[0]} Cr - {netWorth[1]} Cr)
        </MyText>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={netWorth}
          onChange={setNetWorth}
          aria-labelledby='range-slider'
          min={0}
          max={1000}
          marks={[
            { value: 0, label: '0 Cr' },
            { value: 1000, label: '1 Bn' },
          ]}
        />
      </Container>
      <DropDown
        data={ownershipSince}
        header='Owner Since'
        setData={setOwnershipSince}
        LeftIcon={() => (
          <MyImage src='/images/calender.svg' height='20px' width='20px' />
        )}
        OptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/minus.svg' height='15px' width='15px' />
          </>
        )}
        SelectedOptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/plus.svg' height='15px' width='15px' />
          </>
        )}
      />
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '30px' }}>
        Ownership Type
      </MyText>
      <Container styles={{ marginTop: '10px', display: 'flex' }}>
        <HandContainer
          style={{
            border: `1px solid ${
              ownershipType === 1
                ? theme.colors.primary
                : theme.colors.lightGray
            }`,
            marginRight: '10px',
            cursor: 'pointer',
          }}
          onClick={() => handleHandChange(1)}
        >
          <FontAwesomeIcon
            color={
              ownershipType !== 1
                ? theme.colors.lightGray
                : theme.colors.primary
            }
            icon={faUser}
          />
          <MyText
            styles={{
              color:
                ownershipType !== 1
                  ? theme.colors.mediumGray
                  : theme.colors.primary,
              marginLeft: '10px',
            }}
          >
            Individual
          </MyText>
        </HandContainer>
        <HandContainer
          style={{
            border: `1px solid ${
              ownershipType === 2
                ? theme.colors.primary
                : theme.colors.lightGray
            }`,
            marginLeft: '10px',
            cursor: 'pointer',
          }}
          onClick={() => handleHandChange(2)}
        >
          <FontAwesomeIcon
            color={
              ownershipType !== 2
                ? theme.colors.lightGray
                : theme.colors.primary
            }
            icon={faUsers}
          />
          <MyText
            styles={{
              color:
                ownershipType !== 2
                  ? theme.colors.mediumGray
                  : theme.colors.primary,
              marginLeft: '10px',
            }}
          >
            Company / Group
          </MyText>
        </HandContainer>
      </Container>
    </>
  );
};

export default OwnerFilters;
