import React from 'react';
import { Container, MyImage, MyText } from '../../reusables';
import { VenueType } from '../../types';
import { theme } from '../../utils';
import { Item, StatsContainer } from './DataStyles';

export const VenueElement: React.FC<{ item: VenueType; index: number }> = ({
  item,
  index,
}) => {
  return (
    <Item ind={index}>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <MyText
          styles={{
            color: theme.colors.darkPrimary,
            fontWeight: 500,
            fontSize: '18px',
            flex: 2,
          }}
        >
          {item.name}
        </MyText>
        <MyText
          styles={{
            color: theme.colors.darkGray,
            fontSize: '15px',
            fontWeight: 700,
            marginLeft: '5px',
            flex: 1,
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          Team : {item.team}
        </MyText>
      </Container>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: '10px',
        }}
      >
        <StatsContainer>
          <Container styles={{ display: 'flex' }}>
            <MyImage src='/images/stadium.svg' height='15px' width='15px' />
            <MyText
              styles={{
                color: theme.colors.darkGray,
                fontSize: '13px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              Matches : {item.matches}
            </MyText>
          </Container>
        </StatsContainer>
        <MyText styles={{ color: theme.colors.lightGray }}>
          <Container styles={{ display: 'flex', alignItems: 'center' }}>
            Capacity :
            <MyText
              styles={{
                color: theme.colors.mediumGray,
                fontSize: '14px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              {item.capacity}
            </MyText>
          </Container>
        </MyText>
      </Container>
    </Item>
  );
};
