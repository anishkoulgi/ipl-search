## Atlan Task

Create a discovery page for IPL where users can search & use facet filters to find different entities – players, teams, owners & venus. Users should be able to filter based on any attributes related to the entity. Make it as easy, intuitive and as less steps as possible for the user to reach a particular entity.

---

[![UI ](https://img.shields.io/badge/Website-Link-blue?style=for-the-badge&logo=appveyor)](https://ipl-search.netlify.app)

## Technologies Used

This project has been made using React JS (with TypeScript) and styled components as a styling library for CSS in JS.

## Data

I added some data for players from the Kaggle CSV provided. I have added some dummy data for the rest of the categories : Teams, Owners and Venues. The data is present in the `src/utils/data.js` file.

## Approach

- The webpage has been designed in a mobile first approach. Therefore the page is responsive.
- For styling I have used styled components which makes it easy to use CSS in JS.
- TypeScript has been used to avoid common petty bugs and make the code type safe from runtime errors.
- I have mostly used inline styling as it provides a better way to use conditional styles in JS and also syncs up well with styled components.
- The save feature for the filter used `localStorage` for saving the filters as a stringified JSON object.

## Page Load Time

- Page load time in the Chrome's incognito window is 2.89 seconds.
  Initially, as the website had a lot of SVGs (for flags, team logos) it was delaying the paint time. I cleaned and sanitized the SVGs using [SVGOMG](https://jakearchibald.github.io/svgomg/).
- The website was lighthouse audited and here are the results.

  ![Lighthouse Report](./lighthouse-report.png)

- Initially the Accessibility was 57, I added the alt tags for all images and that increased it to 72. The description affects the accessibility performance as React app only has one HTML file with no content except for a single div. This reduces the accessibilty test performance. This can be further improved by using Server Side Rendering (SSR) or using Gatsby or Next.js.
