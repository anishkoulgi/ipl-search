import styled from 'styled-components';

export const JumboContainer = styled.div`
  margin-top: 60px;
  padding: 30px 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
