import React, { useState } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { ThemeProvider } from 'styled-components';
import { AppContainer, BodyContainer } from './AppStyles';
import { Body, Data, Jumbotron, Navbar } from './components';
import { Tabs } from './reusables';
import { CategoryTabs } from './utils';
import { data } from './utils/data';
import { theme } from './utils/theme';

const App = () => {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <ThemeProvider theme={theme}>
      <AppContainer>
        <ToastContainer
          position='top-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
        />
        <Navbar />

        <Jumbotron />
        <Tabs index={tabIndex} setIndex={setTabIndex} data={CategoryTabs} />
        <BodyContainer>
          <Body tab={tabIndex} />
          <Data data={data} index={tabIndex} />
        </BodyContainer>
      </AppContainer>
    </ThemeProvider>
  );
};

export default App;
