import React from 'react';
import { MyText } from '..';
import { theme } from '../../../utils';
import { TabContainer, TabElement } from './TabsStyles';

interface Props {
  index: number;
  data: string[];
  setIndex: React.Dispatch<React.SetStateAction<number>>;
}

const Tabs: React.FC<Props> = ({ index, data, setIndex }) => {
  return (
    <TabContainer>
      {data.map((item, i) => (
        <TabElement
          key={i}
          style={{
            borderBottom: `1px solid ${
              i === index ? theme.colors.primary : theme.colors.lightGray
            }`,
          }}
          onClick={() => setIndex(i)}
        >
          <MyText
            styles={{
              color:
                i === index ? theme.colors.darkPrimary : theme.colors.darkGray,
            }}
          >
            {item}
          </MyText>
        </TabElement>
      ))}
    </TabContainer>
  );
};

export default Tabs;
