import { OwnerFiltersType, TeamFiltersType, VenueFiltersType } from '../types';
import {
  BowlingStyles,
  capacities,
  Countries,
  Matches,
  OwnershipYears,
  Titles,
} from './constants';

export const getCountryFlag = (country: string) => {
  switch (country) {
    case 'India':
      return '/images/india.svg';
    case 'South Africa':
      return '/images/south-africa.svg';
    case 'West Indies':
      return '/images/west-indies.svg';
    case 'Australia':
      return '/images/australia.svg';
    case 'England':
      return '/images/england.svg';
    case 'New Zealand':
      return '/images/new-zealand.svg';
    case 'Bangladesh':
      return '/images/bangladesh.svg';
    case 'Sri Lanka':
      return '/images/sri-lanka.svg';
    case 'Country':
      return '/images/europe.svg';
    default:
      return '/images/no-image.svg';
  }
};

export function getDropDownConfig<T>(data: T[]) {
  let arr = [];
  for (let i = 0; i < data.length; ++i)
    arr.push({
      title: data[i],
      selected: false,
    });
  return arr;
}

export const checkSelected = (arr: { title: string; selected: boolean }[]) => {
  let ans = 0;
  for (let i = 0; i < arr.length; ++i)
    if (arr[i].selected) {
      ans++;
    }
  return ans;
};

export const getPlayerFilterState = () => {
  return {
    countries: getDropDownConfig(Countries),
    age: [20, 30],
    bowlingStyle: getDropDownConfig(BowlingStyles),
    hand: 0,
  };
};

export const getTeamFilterState = (): TeamFiltersType => {
  return {
    titles: getDropDownConfig(Titles),
    matchesPlayed: getDropDownConfig(Matches),
    netWorth: [10, 100],
    winPercentage: [0, 100],
  };
};

export const getOwnerFilterState = (): OwnerFiltersType => {
  return {
    ownershipSince: getDropDownConfig(OwnershipYears),
    netWorth: [10, 100],
    ownershipType: 0,
  };
};

export const getVenueFilterState = (): VenueFiltersType => {
  return {
    capacity: getDropDownConfig(capacities),
    averageScore: [0, 600],
    averageWickets: [0, 20],
    matchesPlayed: getDropDownConfig(Matches),
  };
};

export const getFilterNameFromIndex = (
  idx: number
): 'playerFilters' | 'teamFilters' | 'ownerFilters' | 'venueFilters' => {
  switch (idx) {
    case 0: {
      return 'playerFilters';
    }
    case 1: {
      return 'teamFilters';
    }
    case 2: {
      return 'ownerFilters';
    }
    case 3: {
      return 'venueFilters';
    }
    default: {
      return 'playerFilters';
    }
  }
};

export const getFilterConfigFromIndex = (idx: number) => {
  switch (idx) {
    case 0: {
      return getPlayerFilterState;
    }
    case 1: {
      return getTeamFilterState;
    }
    case 2: {
      return getOwnerFilterState;
    }
    case 3: {
      return getVenueFilterState;
    }
  }
};

export const getDataKeyName = (idx: number) => {
  switch (idx) {
    case 0: {
      return 'players';
    }
    case 1: {
      return 'teams';
    }
    case 2: {
      return 'owners';
    }
    case 3: {
      return 'venues';
    }
    default: {
      return 'players';
    }
  }
};
