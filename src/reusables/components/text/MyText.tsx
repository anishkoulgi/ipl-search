import React from 'react';
import { TextContainer } from './MyTextStyles';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  styles?: React.CSSProperties;
}

const MyText: React.FC<Props> = ({ styles, children, ...rest }) => {
  return (
    <TextContainer style={styles} {...rest}>
      {children}
    </TextContainer>
  );
};

export default MyText;
