import styled from 'styled-components';

export const TextContainer = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Karla:wght@400;500;700');

  font-family: 'Karla', sans-serif;
  font-size: 14px;
  font-display: swap;
`;
