import { faSave, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Modal from 'react-modal';
import { toast } from 'react-toastify';

import { PlayerFilters, TeamFilters } from '.';
import { Container, MyButton, MyText } from '../../reusables';
import {
  FilterStateType,
  OwnerFiltersType,
  PlayerFiltersType,
  TeamFiltersType,
  VenueFiltersType,
} from '../../types';
import {
  getDataKeyName,
  getFilterNameFromIndex,
  getOwnerFilterState,
  getPlayerFilterState,
  getTeamFilterState,
  getVenueFilterState,
  theme,
} from '../../utils';
import { HeaderContainer, ModalContainer } from './FilterElements';
import OwnerFilters from './OwnerFilters/OwnerFilters';
import VenueFilters from './VenueFilters/VenueFilters';

interface Props {
  isOpen: boolean;
  closeModal: () => void;
  tab: number;
  filterState: FilterStateType;
  setFilterState: React.Dispatch<React.SetStateAction<FilterStateType>>;
  setApplied: React.Dispatch<
    React.SetStateAction<{
      players: boolean;
      teams: boolean;
      owners: boolean;
      venues: boolean;
    }>
  >;
}

const FilterModal: React.FC<Props> = ({
  isOpen,
  closeModal,
  tab,
  filterState,
  setFilterState,
  setApplied,
}) => {
  const {
    playerFilters,
    teamFilters,
    ownerFilters,
    venueFilters,
  } = filterState;

  const handlePlayerState = (playerFilters: PlayerFiltersType) => {
    setFilterState((prev) => {
      return {
        ...prev,
        playerFilters,
      };
    });
  };

  const handleTeamState = (teamFilters: TeamFiltersType) => {
    setFilterState((prev) => {
      return {
        ...prev,
        teamFilters,
      };
    });
  };

  const handleOwnerState = (ownerFilters: OwnerFiltersType) => {
    setFilterState((prev) => {
      return {
        ...prev,
        ownerFilters,
      };
    });
  };

  const handleVenueState = (venueFilters: VenueFiltersType) => {
    setFilterState((prev) => {
      return {
        ...prev,
        venueFilters,
      };
    });
  };

  const SelectFilterState = () => {
    switch (tab) {
      case 0: {
        return (
          <PlayerFilters
            playerFilterState={playerFilters}
            setPlayerFilterState={handlePlayerState}
          />
        );
      }
      case 1: {
        return (
          <TeamFilters
            teamFilterState={teamFilters}
            setPlayerFilterState={handleTeamState}
          />
        );
      }
      case 2: {
        return (
          <OwnerFilters
            ownerFilterState={ownerFilters}
            setOwnerFilterState={handleOwnerState}
          />
        );
      }
      case 3: {
        return (
          <VenueFilters
            venueFilterState={venueFilters}
            setVenueFilterState={handleVenueState}
          />
        );
      }
    }
  };

  const clearFilterState = () => {
    switch (tab) {
      case 0: {
        setFilterState((prev) => {
          return {
            ...prev,
            playerFilters: getPlayerFilterState(),
          };
        });
        setApplied((prev) => {
          return {
            ...prev,
            players: false,
          };
        });
        break;
      }
      case 1: {
        setFilterState((prev) => {
          return {
            ...prev,
            teamFilters: getTeamFilterState(),
          };
        });
        setApplied((prev) => {
          return {
            ...prev,
            teams: false,
          };
        });
        break;
      }
      case 2: {
        setFilterState((prev) => {
          return {
            ...prev,
            ownerFilters: getOwnerFilterState(),
          };
        });
        setApplied((prev) => {
          return {
            ...prev,
            owners: false,
          };
        });
        break;
      }
      case 3: {
        setFilterState((prev) => {
          return {
            ...prev,
            venueFilters: getVenueFilterState(),
          };
        });
        setApplied((prev) => {
          return {
            ...prev,
            venues: false,
          };
        });
        break;
      }
    }
  };

  const SaveFilterState = () => {
    const tabName = getFilterNameFromIndex(tab);
    const stringifiedState = JSON.stringify(filterState[tabName]);
    localStorage.setItem(tabName, stringifiedState);
    toast('Saved Filters to local storage!', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      type: 'success',
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={{
        content: {
          height: '75%',
          top: '80px',
          left: '0px',
          right: '0px',
          bottom: '0px',
          display: 'flex',
          justifyContent: 'center',
          backgroundColor: 'transparent',
          border: '0px',
          overflowY: 'hidden',
          padding: '5px',
        },
      }}
    >
      <ModalContainer>
        <HeaderContainer>
          <MyText
            styles={{
              color: theme.colors.darkGray,
              fontSize: '24px',
              fontWeight: 700,
            }}
          >
            Filters
          </MyText>
          <FontAwesomeIcon
            color={theme.colors.mediumGray}
            icon={faTimes}
            style={{
              position: 'absolute',
              top: '5px',
              right: '5px',
              cursor: 'pointer',
            }}
            onClick={closeModal}
          />
        </HeaderContainer>
        {SelectFilterState()}
        <Container styles={{ display: 'flex', marginTop: 'auto' }}>
          <MyButton
            style={{
              flex: 1,
              marginRight: '5px',
              color: theme.colors.mediumGray,
              padding: '10px 0px',
              backgroundColor: 'inherit',
              border: `1px solid ${theme.colors.mediumGray}`,
            }}
            onClick={SaveFilterState}
          >
            <FontAwesomeIcon color={theme.colors.mediumGray} icon={faSave} />
            <MyText styles={{ marginLeft: '5px' }}>Save</MyText>
          </MyButton>
          <MyButton
            style={{
              flex: 1,
              marginRight: '5px',
              marginLeft: '5px',
              color: theme.colors.mediumGray,
              padding: '10px 0px',
              backgroundColor: 'inherit',
              border: `1px solid ${theme.colors.danger}`,
            }}
            onClick={clearFilterState}
          >
            <MyText styles={{ marginLeft: '5px', color: theme.colors.danger }}>
              Clear
            </MyText>
          </MyButton>
          <MyButton
            style={{
              flex: 1,
              marginLeft: '5px',
              color: theme.colors.mediumGray,
              padding: '10px 0px',
              backgroundColor: theme.colors.success,
            }}
            onClick={() => {
              const key = getDataKeyName(tab);
              setApplied((prev) => {
                return {
                  ...prev,
                  [key]: true,
                };
              });
              closeModal();
            }}
          >
            <MyText styles={{ marginLeft: '5px', color: theme.colors.white }}>
              Apply
            </MyText>
          </MyButton>
        </Container>
      </ModalContainer>
    </Modal>
  );
};

export default FilterModal;
