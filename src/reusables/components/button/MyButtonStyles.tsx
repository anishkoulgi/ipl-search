import styled from 'styled-components';

export const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0px 10px;
  background-color: ${(props) => props.theme.colors.primary};
  border-radius: 5px;
`;
