import styled from 'styled-components';

export const SearchContainer = styled.div`
  display: flex;
  border: 1px solid ${(props) => props.theme.colors.veryLightGray};
  height: 40px;
  border-radius: 25px;
  padding-left: 10px;
  align-items: center;
  margin-top: 10px;
  flex: 1;
`;

export const SearchInput = styled.input`
  border: 0;
  background-color: inherit;
  outline: none;
  padding: 0px 10px;
  flex: 1;
  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: ${(props) => props.theme.colors.lightGray};
    font-family: 'Karla', sans-serif;
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color: ${(props) => props.theme.colors.lightGray};
    font-family: 'Karla', sans-serif;
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color: ${(props) => props.theme.colors.lightGray};
    font-family: 'Karla', sans-serif;
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color: ${(props) => props.theme.colors.lightGray};
    font-family: 'Karla', sans-serif;
  }
`;
