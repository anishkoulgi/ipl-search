export { default as MyImage } from './image/MyImage';
export { default as Container } from './container/Container';
export { default as MyText } from './text/MyText';
export { default as Tabs } from './tabs/Tabs';
export { default as MyButton } from './button/MyButton';
export { default as DropDown } from './dropdown/DropDown';
export { default as MySlider } from './slider/MySlider';
