import React, { useEffect, useState } from 'react';
import { MyImage } from '../../reusables';
import { Nav, NavContainer } from './NavbarStyles';

const Navbar = () => {
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  });

  const handleScroll = () => {
    const offset = window.scrollY;
    if (offset > 80) setScrolled(true);
    else setScrolled(false);
  };

  return (
    <NavContainer scrolled={scrolled}>
      <Nav>
        <MyImage src='/images/ipl.svg' height='40px' width='40px' />
        <a href='https://gitlab.com/anishkoulgi/ipl-search'>
          <MyImage src='/images/gitlab.svg' height='40px' width='40px' />
        </a>
      </Nav>
    </NavContainer>
  );
};

export default Navbar;
