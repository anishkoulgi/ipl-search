import React from 'react';
import { Button } from './MyButtonStyles';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  textStyle?: React.CSSProperties;
}

const MyButton: React.FC<Props> = ({ children, style, textStyle, ...rest }) => {
  return (
    <Button style={{ cursor: 'pointer', ...style }} {...rest}>
      {children}
    </Button>
  );
};

export default MyButton;
