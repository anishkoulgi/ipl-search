import { withStyles } from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import React from 'react';
import { Container, DropDown, MyImage, MyText } from '../../../reusables';
import { dropdown } from '../../../reusables/components/dropdown/DropDown';
import { VenueFiltersType } from '../../../types';
import { theme } from '../../../utils';

export const MySlider = withStyles({
  root: {
    color: theme.colors.primary,
    height: 4,
    fontSize: '13px',
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50%)',
    fontFamily: 'Karla, sans-serif',
  },
  track: {
    height: 4,
    borderRadius: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
  markLabel: {
    fontSize: '11px',
    color: theme.colors.mediumGray,
  },
})(Slider);

interface Props {
  venueFilterState: VenueFiltersType;
  setVenueFilterState: (venueFilters: VenueFiltersType) => void;
}

const VenueFilters: React.FC<Props> = ({
  venueFilterState,
  setVenueFilterState,
}) => {
  const {
    averageScore,
    averageWickets,
    capacity,
    matchesPlayed,
  } = venueFilterState;

  const setCapacity = (val: dropdown[]) => {
    setVenueFilterState({
      ...venueFilterState,
      capacity: val,
    });
  };

  const setAverageScore = (e: any, val: any) => {
    setVenueFilterState({
      ...venueFilterState,
      averageScore: val,
    });
  };

  const setMatchesPlayed = (val: dropdown[]) => {
    setVenueFilterState({
      ...venueFilterState,
      matchesPlayed: val,
    });
  };

  const setAverageWickets = (e: any, val: any) => {
    setVenueFilterState({
      ...venueFilterState,
      averageWickets: val,
    });
  };

  return (
    <>
      <Container styles={{ marginTop: '30px', marginBottom: '20px' }}>
        <DropDown
          data={capacity}
          header='Venue Capacity'
          setData={setCapacity}
          LeftIcon={() => (
            <MyImage src='/images/stadium.svg' height='20px' width='20px' />
          )}
          OptionsComponent={({ item, index }) => (
            <>
              <Container styles={{ display: 'flex', alignItems: 'center' }}>
                <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              </Container>
              <MyImage src='/images/minus.svg' height='15px' width='15px' />
            </>
          )}
          SelectedOptionsComponent={({ item, index }) => (
            <>
              <Container styles={{ display: 'flex', alignItems: 'center' }}>
                <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              </Container>
              <MyImage src='/images/plus.svg' height='15px' width='15px' />
            </>
          )}
        />
      </Container>
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '20px' }}>
        Average Runs Scored ({averageScore[0]} runs - {averageScore[1]} runs)
      </MyText>
      <Container styles={{ padding: '0px 15px', margin: '10px 0px 20px 0px' }}>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={averageScore}
          onChange={setAverageScore}
          aria-labelledby='range-slider'
          min={0}
          max={600}
          marks={[
            { value: 0, label: '0 Runs' },
            { value: 600, label: '600 Runs' },
          ]}
        />
      </Container>
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '20px' }}>
        Average Wickets taken ({averageWickets[0]} - {averageWickets[1]} )
      </MyText>
      <Container styles={{ padding: '0px 15px', margin: '10px 0px' }}>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={averageWickets}
          onChange={setAverageWickets}
          aria-labelledby='range-slider'
          min={0}
          max={20}
          marks={[
            { value: 0, label: '0' },
            { value: 20, label: '20' },
          ]}
        />
      </Container>
      <Container styles={{ margin: '20px 0px' }}>
        <DropDown
          data={matchesPlayed}
          header='Matches played'
          setData={setMatchesPlayed}
          LeftIcon={() => (
            <MyImage src='/images/matches.svg' height='20px' width='20px' />
          )}
          OptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/minus.svg' height='15px' width='15px' />
            </>
          )}
          SelectedOptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/plus.svg' height='15px' width='15px' />
            </>
          )}
        />
      </Container>
    </>
  );
};

export default VenueFilters;
