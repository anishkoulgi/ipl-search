import React from 'react';
import { Container, MyImage, MyText } from '../../reusables';
import { TeamType } from '../../types';
import { theme } from '../../utils';
import { Item, StatsContainer } from './DataStyles';

export const TeamElement: React.FC<{ item: TeamType; index: number }> = ({
  item,
  index,
}) => {
  return (
    <Item ind={index}>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <MyText
          styles={{
            color: theme.colors.darkPrimary,
            fontWeight: 500,
            fontSize: '18px',
          }}
        >
          {item.name}
        </MyText>
        <MyImage src={item.image} height='30px' width='30px' />
      </Container>
      <Container
        styles={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: '10px',
        }}
      >
        <StatsContainer style={{ flex: 1 }}>
          <Container styles={{ display: 'flex' }}>
            <MyImage src='/images/trophy.svg' height='15px' width='15px' />
            <MyText
              styles={{
                color: theme.colors.darkGray,
                fontSize: '13px',
                fontWeight: 500,
                marginLeft: '5px',
              }}
            >
              Titles Won : {item.titles}
            </MyText>
          </Container>
        </StatsContainer>
        <MyText
          styles={{
            color: theme.colors.lightGray,
            flex: 1,
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <MyText
            styles={{
              color: theme.colors.mediumGray,
              fontSize: '14px',
              fontWeight: 500,
              marginLeft: '5px',
            }}
          >
            Owner : {item.owner}
          </MyText>
        </MyText>
      </Container>
    </Item>
  );
};
