import styled from 'styled-components';

export const FilterContainer = styled.div`
  @media (min-width: 576px) {
    display: none;
  }
`;

export const BodyContainer = styled.div`
  max-width: 1200px;
  width: 90%;
  margin-top: 20px;
`;
export const SearchContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (min-width: 576px) {
    flex-direction: row;
  }
`;

export const FilterDesktopContainer = styled.div`
  display: flex;
  align-items: center;
`;
