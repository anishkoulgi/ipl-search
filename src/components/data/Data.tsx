import React from 'react';
import { Container } from '../../reusables';
import { dataType } from '../../types';
import { getDataKeyName } from '../../utils';
import { ItemContainer } from './DataStyles';
import { OwnerElement } from './OwnerElement';
import { PlayerElement } from './PlayerElement';
import { TeamElement } from './TeamElement';
import { VenueElement } from './VenueElements';

interface Props {
  data: dataType;
  index: number;
}

const getDataElement = (idx: number, tab: number, item: any) => {
  switch (tab) {
    case 0: {
      return <PlayerElement item={item} index={idx} />;
    }
    case 1: {
      return <TeamElement item={item} index={idx} />;
    }
    case 2: {
      return <OwnerElement item={item} index={idx} />;
    }
    case 3: {
      return <VenueElement item={item} index={idx} />;
    }
    default: {
      return 'players';
    }
  }
};

const Data: React.FC<Props> = ({ data, index }) => {
  return (
    <Container
      styles={{ marginTop: '20px', maxWidth: '1200px', width: '100%' }}
    >
      {(data[getDataKeyName(index)] as any).map((item: any, i: number) => {
        if (i % 2 === 0)
          return (
            <ItemContainer>
              {getDataElement(i, index, item)}
              {i !== data[getDataKeyName(index)].length &&
                getDataElement(i, index, data[getDataKeyName(index)][i + 1])}
            </ItemContainer>
          );
        return null;
      })}
    </Container>
  );
};

export default Data;
