import React, { ImgHTMLAttributes } from 'react';
import { Image } from './MyImageStyles';

interface Props extends ImgHTMLAttributes<HTMLImageElement> {
  height?: string;
  width?: string;
  styles?: React.CSSProperties;
}

const MyImage: React.FC<Props> = ({ height, width, styles, ...rest }) => {
  return (
    <Image
      alt='Image not loaded'
      style={{ height, width, ...styles }}
      {...rest}
    />
  );
};

export default MyImage;
