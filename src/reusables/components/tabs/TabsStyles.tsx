import styled from 'styled-components';

export const TabContainer = styled.div`
  display: flex;
  width: 100%;
  max-width: 1200px;
`;

export const TabElement = styled.div`
  flex: 1;

  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 0px;
  transition: all 200ms;
  cursor: pointer;
`;
