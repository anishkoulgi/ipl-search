// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React, { useEffect, useRef, useState } from 'react';

export const useOutsideClick = () => {
  const node = useRef<HTMLDivElement>(null);

  const [open, setOpen] = useState(false);

  const handleClickOutside = (e: any) => {
    const curr = node.current;
    if (curr && curr.contains(e.target)) {
      // inside click
      return;
    }
    // outside click
    setOpen(false);
  };

  useEffect(() => {
    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open]);

  return {
    node,
    open,
    setOpen,
  };
};
