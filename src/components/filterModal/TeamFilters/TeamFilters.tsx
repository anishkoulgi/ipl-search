import { withStyles } from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import React from 'react';
import { Container, DropDown, MyImage, MyText } from '../../../reusables';
import { dropdown } from '../../../reusables/components/dropdown/DropDown';
import { TeamFiltersType } from '../../../types';
import { theme } from '../../../utils';

export const MySlider = withStyles({
  root: {
    color: theme.colors.primary,
    height: 4,
    fontSize: '13px',
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50%)',
    fontFamily: 'Karla, sans-serif',
  },
  track: {
    height: 4,
    borderRadius: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
  markLabel: {
    fontSize: '11px',
    color: theme.colors.mediumGray,
  },
})(Slider);

interface Props {
  teamFilterState: TeamFiltersType;
  setPlayerFilterState: (playerFilters: TeamFiltersType) => void;
}

const TeamFilters: React.FC<Props> = ({
  teamFilterState,
  setPlayerFilterState,
}) => {
  const { titles, netWorth, matchesPlayed, winPercentage } = teamFilterState;

  const setTitles = (val: dropdown[]) => {
    setPlayerFilterState({
      ...teamFilterState,
      titles: val,
    });
  };

  const setNetWorth = (e: any, val: any) => {
    setPlayerFilterState({
      ...teamFilterState,
      netWorth: val,
    });
  };

  const setMatchesPlayed = (val: dropdown[]) => {
    setPlayerFilterState({
      ...teamFilterState,
      matchesPlayed: val,
    });
  };

  const setWinPercentage = (e: any, val: any) => {
    setPlayerFilterState({
      ...teamFilterState,
      winPercentage: val,
    });
  };

  return (
    <>
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '20px' }}>
        Net Worth ({netWorth[0]} Cr - {netWorth[1]} Cr)
      </MyText>
      <Container styles={{ padding: '0px 15px', margin: '10px 0px 20px 0px' }}>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={netWorth}
          onChange={setNetWorth}
          aria-labelledby='range-slider'
          min={10}
          max={100}
          marks={[
            { value: 10, label: '10 Cr' },
            { value: 100, label: '100 Cr' },
          ]}
        />
      </Container>
      <DropDown
        data={titles}
        header='IPL Titles'
        setData={setTitles}
        LeftIcon={() => (
          <MyImage src='/images/trophy.svg' height='20px' width='20px' />
        )}
        OptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/minus.svg' height='15px' width='15px' />
          </>
        )}
        SelectedOptionsComponent={({ item, index }) => (
          <>
            <Container styles={{ display: 'flex', alignItems: 'center' }}>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
            </Container>
            <MyImage src='/images/plus.svg' height='15px' width='15px' />
          </>
        )}
      />
      <Container styles={{ marginTop: '30px' }}>
        <DropDown
          data={matchesPlayed}
          header='Matches played'
          setData={setMatchesPlayed}
          LeftIcon={() => (
            <MyImage src='/images/matches.svg' height='20px' width='20px' />
          )}
          OptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/minus.svg' height='15px' width='15px' />
            </>
          )}
          SelectedOptionsComponent={({ item, index }) => (
            <>
              <MyText styles={{ marginLeft: '10px' }}>{item.title}</MyText>
              <MyImage src='/images/plus.svg' height='15px' width='15px' />
            </>
          )}
        />
      </Container>
      <MyText styles={{ color: theme.colors.mediumGray, marginTop: '30px' }}>
        Win Percentage ({winPercentage[0]} % - {winPercentage[1]} %)
      </MyText>
      <Container styles={{ padding: '0px 15px', margin: '10px 0px' }}>
        <MySlider
          valueLabelDisplay='auto'
          aria-label='pretto slider'
          defaultValue={20}
          value={winPercentage}
          onChange={setWinPercentage}
          aria-labelledby='range-slider'
          min={0}
          max={100}
          marks={[
            { value: 0, label: '0%' },
            { value: 100, label: '100%' },
          ]}
        />
      </Container>
    </>
  );
};

export default TeamFilters;
