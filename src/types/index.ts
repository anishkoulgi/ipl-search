export interface PlayersType {
  Player_Name: string;
  DOB: string;
  Batting_Hand: string;
  Bowling_Skill: string;
  Country: string;
}

export interface OwnerType {
  name: string;
  type: string;
  ownerSince: number;
  netWorth: string;
}

export interface TeamType {
  name: string;
  image: string;
  titles: number;
  owner: string;
}

export interface VenueType {
  name: string;
  team: string;
  matches: number;
  capacity: number;
}

export interface dataType {
  players: PlayersType[];
  owners: OwnerType[];
  teams: TeamType[];
  venues: VenueType[];
}

export type ItemType = PlayersType | TeamType | OwnerType | VenueType;

export interface PlayerFiltersType {
  countries: {
    title: string;
    selected: boolean;
  }[];
  age: number[];
  bowlingStyle: {
    title: string;
    selected: boolean;
  }[];
  hand: number;
}

export type TeamFiltersType = {
  titles: {
    title: string;
    selected: boolean;
  }[];
  netWorth: number[];
  winPercentage: number[];
  matchesPlayed: {
    title: string;
    selected: boolean;
  }[];
};

export type OwnerFiltersType = {
  ownershipSince: {
    title: string;
    selected: boolean;
  }[];
  netWorth: number[];
  ownershipType: number;
};

export type VenueFiltersType = {
  capacity: {
    title: string;
    selected: boolean;
  }[];
  averageScore: number[];
  averageWickets: number[];
  matchesPlayed: {
    title: string;
    selected: boolean;
  }[];
};

export interface FilterStateType {
  playerFilters: PlayerFiltersType;
  teamFilters: TeamFiltersType;
  ownerFilters: OwnerFiltersType;
  venueFilters: VenueFiltersType;
}
